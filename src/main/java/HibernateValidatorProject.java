import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import javax.validation.Validation;
import javax.validation.Validator;

public class HibernateValidatorProject {

    public static void main(String[] args) {


        HibernateValidatorConfiguration configuration = Validation
                .byProvider( HibernateValidator.class )
                .configure();


        ConstraintMapping constraintMapping = configuration.createConstraintMapping();

        constraintMapping
                .type( Car.class )
                .field( "manufacturer" )
                .constraint( new NotNullDef() )
                .field( "licensePlate" )
                .ignoreAnnotations( true )
                .constraint( new NotNullDef() )
                .constraint( new SizeDef().min( 2 ).max( 14 ));

        Validator validatorz = configuration.addMapping( constraintMapping )
                .buildValidatorFactory()
                .getValidator();

    }

}
